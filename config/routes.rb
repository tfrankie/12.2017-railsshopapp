Rails.application.routes.draw do

  devise_for :users, :controllers => { registrations: 'devise/user_registrations' }
  devise_for :admins

  root 'common/events#index'
  
  get '/admins', to: redirect('/admins/events')
  namespace :admins do
    resources :events
    resources :users
    resources :tickets
    resources :refunds
    post 'refund/:id/verify' => 'refunds#verify', as: :verify_refund
  end

  get '/users', to: redirect('/users/tickets')
  namespace :users do
    resources :tickets
    get 'events' => 'events#index', as: :events
    post 'events/:id/order_tickets' => 'tickets#order_tickets', as: :order_tickets
    post 'events/:id/purchase_tickets' => 'tickets#purchase_tickets', as: :purchase_tickets
    post 'tickets/:id/return' => 'tickets#return_ticket', as: :return_ticket
  end

  scope module: 'common' do
    get 'events' => 'events#index', as: :events
    post 'events' => 'events#index', as: :filter_events
    get 'events/:id' => 'events#show', as: :event
  end

end