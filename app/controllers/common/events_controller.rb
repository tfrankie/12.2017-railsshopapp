module Common
  class EventsController < CommonController

    # GET/POST /events
    # GET/POST /events.json
    def index
      if request.post?
        dateFilters = params[:date_filter]
        @from = dateFilters[:from]
        @to = dateFilters[:to]
        if ( @from.present? && @to.present? )
          @events = Event.where('event_date between (?) and (?)', @from, @to).order(:event_date => :desc)
        else
          @events = Event.where('event_date > ?', Date.today - 1.day).order(:event_date => :asc)
        end
      else
        @events = Event.where('event_date > ?', Date.today - 1.day).order(:event_date => :asc)
      end
    end

    # GET /events/1
    # GET /events/1.json
    def show
      @event = Event.find(params[:id])
    end
  
  end  
end

