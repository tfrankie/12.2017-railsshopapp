module Admins
  class TicketsController < AdminController
    before_action :set_ticket, only: [:show, :edit, :update, :destroy]
  
    # GET /tickets
    # GET /tickets.json
    def index
      @allTickets = Ticket.all
      @activeTickets = @allTickets.select{ |ticket| ticket.event[:event_date] > Date.today - 1.day }
      @expiredTickets =  @allTickets.select{ |ticket| ticket.event[:event_date] < Date.today - 1.day }     
    end
  
    # GET /tickets/1
    # GET /tickets/1.json
    def show
    end
  
    # GET /tickets/new
    def new
      @ticket = Ticket.new
      @ticket.event_id = params[:eventId]
    end
  
    # GET /tickets/1/edit
    def edit
    end
  

    # POST /tickets
    # POST /tickets.json
    def create
      @ticket = Ticket.new(ticket_params)
  
      respond_to do |format|
        if @ticket.save
          format.html { redirect_to admins_event_path(@ticket.event_id), notice: 'Ticket was successfully created.' }
          format.json { render :show, status: :created, location: @ticket }
        else
          format.html { render :new }
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # PATCH/PUT /tickets/1
    # PATCH/PUT /tickets/1.json
    def update
      respond_to do |format|
        if @ticket.update(ticket_params)
          format.html { redirect_to admins_tickets_path, notice: 'Ticket was successfully updated.' }
          format.json { render :show, status: :ok, location: @ticket }
        else
          format.html { render :edit }
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # DELETE /tickets/1
    # DELETE /tickets/1.json
    def destroy
      @ticket.destroy
      respond_to do |format|
        format.html { redirect_to admins_tickets_path, notice: 'Ticket was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_ticket
        @ticket = Ticket.find(params[:id])
      end
  
      # Never trust parameters from the scary internet, only allow the white list through.
      def ticket_params
        params.require(:ticket).permit(:seat_id, :price, :user_id, :event_id)
      end
  end  
end
