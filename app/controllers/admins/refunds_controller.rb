module Admins
  class RefundsController < AdminController
    before_action :set_refund, only: [:show, :edit, :update, :destroy, :verify]
  
    # GET /refunds
    # GET /refunds.json
    def index
      @refundRequests = Refund.where(verified: false)
      @verifiedRequests = Refund.where(verified: true)
    end
  
    # GET /refunds/1
    # GET /refunds/1.json
    def show
    end
  
    # GET /refunds/new
    def new
      @refund = Refund.new
    end
  
    # GET /refunds/1/edit
    def edit
    end

    # POST - verify ticket
    def verify
      @refund.verified = true
      @refund.save

      @ticket = @refund.ticket
      @ticket.user = nil
      @ticket.save

      respond_to do |format|
        format.html { redirect_to admins_refunds_path, notice: 'Refund verified' }
      end
    end
  
    # POST /refunds
    # POST /refunds.json
    def create
      @refund = Refund.new(refund_params)
  
      respond_to do |format|
        if @refund.save
          format.html { redirect_to admins_refund_path(@refund), notice: 'Refund was successfully created.' }
          format.json { render :show, status: :created, location: @refund }
        else
          format.html { render :new }
          format.json { render json: @refund.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # PATCH/PUT /refunds/1
    # PATCH/PUT /refunds/1.json
    def update
      respond_to do |format|
        if @refund.update(refund_params)
          format.html { redirect_to admins_refund_path(@refund), notice: 'Refund was successfully updated.' }
          format.json { render :show, status: :ok, location: @refund }
        else
          format.html { render :edit }
          format.json { render json: @refund.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # DELETE /refunds/1
    # DELETE /refunds/1.json
    def destroy
      @refund.destroy
      respond_to do |format|
        format.html { redirect_to admins_refunds_url, notice: 'Refund was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_refund
        @refund = Refund.find(params[:id])
      end
  
      # Never trust parameters from the scary internet, only allow the white list through.
      def refund_params
        params.require(:refund).permit(:ticket_id, :user_id)
      end
  end  
end