module Users
  class EventsController < UserController
  
    # GET /events
    # GET /events.json
    def index
      @allEvents = Ticket.where(user: current_user).map {|ticket| ticket.event}.uniq! {|e| e }
      @incomingEvents = @allEvents.select{ |event| event[:event_date] > Date.today - 1.day }
      @expiredEvents =  @allEvents.select{ |event| event[:event_date] < Date.today - 1.day }
    end

  end  
end
