module Users
  class TicketsController < UserController
  
    # GET /tickets
    # GET /tickets.json
    def index
      @allTickets = Ticket.where(user: current_user)
      @activeTickets = @allTickets.select{ |ticket| ticket.event[:event_date] > Date.today - 1.day }
      @expiredTickets =  @allTickets.select{ |ticket| ticket.event[:event_date] < Date.today - 1.day }     
    end

    # GET
    def order_tickets
      if(params[:selected_tickets] == nil)
        redirect_to event_path(params[:id]), notice: 'Select tickets.'
      elsif(params[:selected_tickets].length > 5)
        redirect_to event_path(params[:id]), notice: 'You can select max 5 tickets.'
      elsif(params[:selected_tickets].length + Ticket.where(user: current_user, event: params[:id]).length > 5)
        redirect_to event_path(params[:id]), notice: 'You can buy max 5 tickets on single event.'
      else
        @selected_tickets = Ticket.where(id: params[:selected_tickets])
      end
    end

    #POST
    def purchase_tickets
      @selected_tickets = Ticket.where(id: params[:selected_tickets])
      @selected_tickets.each do |ticket|
        ticket.user = current_user
        ticket.save
      end
      respond_to do |format|
        format.html { redirect_to events_path, notice: 'Ticket bought.' }
      end
    end

    # POST
    def return_ticket
      @ticket = Ticket.find(params[:id])
      @refund = Refund.new
      @refund.user = current_user
      @refund.ticket = @ticket

      publishing_duration_days = @ticket.event.event_date.minus_with_duration(@ticket.event["created_at"].to_datetime).round
      days_to_event = @ticket.event.event_date.minus_with_duration(Date.today).round
      days_to_event_in_percentage = 100 * days_to_event / publishing_duration_days

      @refund.percents = 100 - ((100 - days_to_event_in_percentage)/2 + 10)
      @refund.price = @refund.percents * @ticket.price / 100
      
      @refund.save

      respond_to do |format|
        format.html { redirect_to users_tickets_path, notice: 'Ticket refund request sent. Refund have to be accepted by administrator.' }
      end
    end
    
  end  
end
