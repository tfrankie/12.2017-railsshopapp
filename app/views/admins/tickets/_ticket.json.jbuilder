json.extract! ticket, :id, :seat_id, :price, :user_id, :event_id, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
