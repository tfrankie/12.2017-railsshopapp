class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tickets
  validates :born, presence: true
  validate :born_date_cannot_be_in_the_future

  def born_date_cannot_be_in_the_future
    if born.present? && born > Date.today
      errors.add(:born, "can't be in the future.")
    end
  end  
end
