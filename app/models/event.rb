class Event < ApplicationRecord
    has_many :tickets, dependent: :delete_all

    validates :name, presence: true, length: {minimum: 3, maximum: 150}
    validates :event_date, presence: true
    validate :tickets_publishing_cannot_be_in_the_past 
    validate :tickets_publishing_cannot_be_after_event_date
    validate :event_date_cannot_be_in_the_past

    def tickets_publishing_cannot_be_in_the_past
      if published.present? && published < Date.today
        errors.add(:published, "can't be in the past.")
      end
    end

    def event_date_cannot_be_in_the_past
        if event_date.present? && event_date < Date.today
          errors.add(:event_date, "can't be in the past.")
        end
      end
    
    def tickets_publishing_cannot_be_after_event_date
        if published.present? && published > event_date
          errors.add(:published, "have to be before event date.")
        end
      end    
end