class AddPercentsToRefunds < ActiveRecord::Migration[5.1]
  def change
    add_column :refunds, :percents, :integer
  end
end
