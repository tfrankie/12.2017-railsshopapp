class AddVerifiedToRefunds < ActiveRecord::Migration[5.1]
  def change
    add_column :refunds, :verified, :boolean, default: false
  end
end
