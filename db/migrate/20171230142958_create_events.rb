class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.string :photo
      t.text :description
      t.integer :minAge
      t.date :published
      t.date :event_date

      t.timestamps
    end
  end
end
