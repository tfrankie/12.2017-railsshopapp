class AddPriceToRefunds < ActiveRecord::Migration[5.1]
  def change
    add_column :refunds, :price, :decimal
  end
end
